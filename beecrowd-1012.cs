using System;

class exercise3
{
   static void Main()
   {
      double A, B, C;
      A = double.Parse(Console.ReadLine());
      B = double.Parse(Console.ReadLine());
      C = double.Parse(Console.ReadLine());

      double triangleResult = 1.0 / 2 * A * C;
      double circleResult = 3.14159 * Math.Pow(C, 2);
      double trapeziumResult = 1.0 / 2 * (A + B) * C;
      double squareResult = Math.Pow(B, 2);
      double rectangleResult = A * B;

      Console.WriteLine($"TRIANGULO: {triangleResult.ToString("F3")}");
      Console.WriteLine($"CIRCULO: {circleResult.ToString("F3")}");
      Console.WriteLine($"TRAPEZIO: {trapeziumResult.ToString("F3")}");
      Console.WriteLine($"QUADRADO: {squareResult.ToString("F3")}");
      Console.WriteLine($"RETANGULO: {rectangleResult.ToString("F3")}");
   }
}
