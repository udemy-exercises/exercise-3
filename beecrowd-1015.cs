using System;

class exercise3
{
   static void Main()
   {
      (double x1, double y1) tuple1;
      (double x2, double y2) tuple2;

      tuple1.x1 = double.Parse(Console.ReadLine());
      tuple1.y1 = double.Parse(Console.ReadLine());
      tuple2.x2 = double.Parse(Console.ReadLine());
      tuple2.y2 = double.Parse(Console.ReadLine());

      double result = Math.Sqrt(Math.Pow((tuple2.x2 - tuple1.x1), 2) + Math.Pow((tuple2.y2 - tuple1.y1), 2));

      Console.WriteLine(result.ToString("F4"));
   }
}
