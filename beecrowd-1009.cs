using System;

class exercise3
{
   static void Main()
   {
      string name;
      double salary, sales, result;

      name = Console.ReadLine();
      salary = double.Parse(Console.ReadLine());
      sales = double.Parse(Console.ReadLine());
      result = salary + (sales * 15 /100);

      Console.WriteLine($"TOTAL = R$ {result.ToString("F2")}");
   }
}
