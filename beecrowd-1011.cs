using System;

class exercises3
{
   static void Main()
   {
      const double fourBythree = 4.0 / 3;
      const double pi = 3.14159;
      double R = double.Parse(Console.ReadLine());
      double result = fourBythree * pi * Math.Pow(R, 3);

      Console.WriteLine($"VOLUME = {result.ToString("F3")}");
   }
}
